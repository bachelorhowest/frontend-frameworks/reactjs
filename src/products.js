
import React, {Component} from 'react';

export class Product extends Component
{

    render() {
        const products  = this.props.data;
        return (
            <div>
                <table className="display" width="100%" id="table">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>articleId</th>
                        <th>price</th>
                        <th>width</th>
                        <th>height</th>
                        <th>description</th>
                        <th> created at</th>
                        <th> updated at</th>
                    </tr>
                    </thead>
                    <tbody>
                    {products.map(product =>
                    <tr>
                        <td> {product.id} </td>
                        <td> {product.name} </td>
                        <td> {product.articleID} </td>
                        <td> {product.price} </td>
                        <td> {product.width} </td>
                        <td> {product.height} </td>
                        <td> {product.description}</td>
                        <td> {product.created_at}</td>
                        <td> { product.updated_at}</td>
                    </tr>)}
                    </tbody>
                </table>
            </div>
        );
    }


}

export default Product;