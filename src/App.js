import React, {Component} from 'react';
import Products from './products'
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom'


class App extends Component
{
    constructor(props) {
        super(props);
        this.state = { products: [], loading: true };
        this.getProducts = this.getProducts.bind(this);
    }

    componentDidMount() {
        this.getProducts();
    }

    getProducts (){
        fetch("http://34.123.200.254/products", {
            "method": "GET",
            "headers": {
                "content-type": "application/json",
                "accept": "application/json"
            },
        })
            .then(response => response.json())
            .then(response => {
                this.setState({ products: response.data});
                this.setState({ loading: false});
            })
            .catch(err => {
                console.log(err);
            });
    }



    render() {
        let toRender = (<p>Loading</p>);

        if (!this.state.loading) {
            toRender = (<Products data={this.state.products} />)
        }

        return (
            <div>
                {toRender}
            </div>
        );
    }
}

export default App;